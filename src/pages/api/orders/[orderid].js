const axios = require("axios");

export default async (req, res) => {
    const {query: { orderid }} = req;

    const ret = await axios.get(`https://ohmonsapin.fr/wp-json/wc/v3/orders/${orderid}`, {
        params: {
          consumer_key: "ck_500475d0e0e96fb8890824dd4c3f257b86ec8612",
          consumer_secret: "cs_8d33c37511d648ab80c66d77f6b5a47ce176e270"
        }
      })
      res.send(ret.data);
  }