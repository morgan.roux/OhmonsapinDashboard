const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;
const axios = require("axios");

  export default async (req, res) => {
    if (req.method === 'PUT') {
      const { id, status } = req.query;
      try {
        const ret = await axios.put(`https://ohmonsapin.fr/wp-json/wc/v3/orders/${id}?consumer_key=ck_500475d0e0e96fb8890824dd4c3f257b86ec8612&consumer_secret=cs_8d33c37511d648ab80c66d77f6b5a47ce176e270&status=${status}`, {
          // params: {
          //   // consumer_key: "ck_500475d0e0e96fb8890824dd4c3f257b86ec8612",
          //   // consumer_secret: "cs_8d33c37511d648ab80c66d77f6b5a47ce176e270",
          //   status: "completed"
          // }
        })
        res.send(ret.data);
      } catch(e) {
        res.send(e);
      }
    }
  };